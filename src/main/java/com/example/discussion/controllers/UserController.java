package com.example.discussion.controllers;

import com.example.discussion.exceptions.UserException;
import com.example.discussion.models.User;
import com.example.discussion.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;

    //Create a user
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<Object> createUser(@RequestBody User user){
        userService.createUser(user);
        return new ResponseEntity<>("User created successfully!", HttpStatus.CREATED);
    }

    //Get user
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ResponseEntity<Object> getUsers(){
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    //Delete user
    @RequestMapping(value = "/users/{userid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteUser(@PathVariable Long userid) {
        return userService.deleteUser(userid);
    }

    //Update a user
    @RequestMapping(value = "/users/{userid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateUser(@PathVariable Long userid, @RequestBody User user) {
        return userService.updateUser(userid, user);
    }

    //User registration
    @RequestMapping(value = "/users/register", method = RequestMethod.POST)

    //Register method tales a request body as a Map of key-value pairs where the keys are strings and the values are also strings. it also throws a UserException in case of an error
    public ResponseEntity<Object> register(@RequestBody Map<String, String> body) throws UserException {

        //This retrieves the value associated with the username key from the request body "Map" and assigns it to a String variable called "username"
        String username = body.get("username");

        //Checks if the user provided "username" exists in the database. If the user exists, it throws a UserException with the message "Username already exists"
        if(!userService.findByUsername(username).isEmpty()){
            throw new UserException("Username already exists.");
        }
        //if username does not exist, it will proceed on creating the client
        else {
            //This retrieves the value associated with the "password" key from the request body "Map" and assigns it to a String variable called "password"
            String password = body.get("password");

            //This encrypts the password using the BCryptPasswordEncoder and store it to the "encodedPassword" variable
            String encodedPassword = new BCryptPasswordEncoder().encode(password);

            //Instantiate the User model to create a new user
            User newUser = new User(username, encodedPassword);

            //save in the database
            userService.createUser(newUser);

            //Send a "User registered successfully" message as the response body and an HTTP status code of 201
            return new ResponseEntity<>("User registered successfully!", HttpStatus.CREATED);
        }


    }
}
